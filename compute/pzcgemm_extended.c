/**
 *
 * @file
 *
 *  PLASMA is a software package provided by:
 *  University of Tennessee, US,
 *  University of Manchester, UK.
 *
 * @precisions mixed zc -> ds
 *
 **/

#include "plasma_async.h"
#include "plasma_context.h"
#include "plasma_descriptor.h"
#include "plasma_internal.h"
#include "plasma_types.h"
#include "plasma_workspace.h"
#include "core_blas.h"

#define A(m, n) (plasma_complex32_t*)plasma_tile_addr(A, m, n)
#define B(m, n) (plasma_complex32_t*)plasma_tile_addr(B, m, n)
#define C(m, n) (plasma_complex32_t*)plasma_tile_addr(C, m, n)
#define D(m, n) (plasma_complex64_t*)plasma_tile_addr(D, m, n)

/***************************************************************************//**
 * Parallel tile matrix-matrix multiplication.
 * @see plasma_omp_zgemm
 ******************************************************************************/
void plasma_pzcgemm_extended(plasma_enum_t transa, plasma_enum_t transb,
                   plasma_complex32_t alpha, plasma_desc_t A,
                                             plasma_desc_t B,
                   plasma_complex64_t beta,  plasma_desc_t C, plasma_desc_t D, int freq,
                   plasma_sequence_t *sequence, plasma_request_t *request)
{
    // Return if failed sequence.
    if (sequence->status != PlasmaSuccess)
        return;

    // This flag indicates whether compensation has just been performed
    int flagXP;

    for (int m = 0; m < C.mt; m++) {
        int mvcm = plasma_tile_mview(C, m);
        int ldcm = plasma_tile_mmain(C, m);
        for (int n = 0; n < C.nt; n++) {
            int nvcn = plasma_tile_nview(C, n);

            // Reset flag before each loop on k
            flagXP = 0;

            //=========================================
            // alpha*A*B does not contribute; scale C
            //=========================================
            int inner_k = transa == PlasmaNoTrans ? A.n : A.m;
            if (alpha == 0.0 || inner_k == 0) {
                int ldam = imax(1, plasma_tile_mmain(A, 0));
                int ldbk = imax(1, plasma_tile_mmain(B, 0));
                // will just do scaling but needs to be on D !!!
                core_omp_zcgemm_extended(
                    transa, transb,
                    mvcm, nvcn, 0,
                    alpha, A(0, 0), ldam,
                           B(0, 0), ldbk,
                    beta,  D(m, n), ldcm,
                    C(m, n), ldcm, 
                    sequence, request);
            }
            else if (transa == PlasmaNoTrans) {
                int ldam = plasma_tile_mmain(A, m);
                //================================
                // PlasmaNoTrans / PlasmaNoTrans
                //================================
                if (transb == PlasmaNoTrans) {
                    for (int k = 0; k < A.nt; k++) {
                        int nvak = plasma_tile_nview(A, k);
                        int ldbk = plasma_tile_mmain(B, k);
                        plasma_complex64_t zbeta = k == 0 ? beta : 1.0;

                        // If extended precision was used in previous sum
                        // then start with a fresh accumulator by setting beta=0
                        plasma_complex32_t cbetaloc = (flagXP) ? 0.f : (float)zbeta;
                        plasma_complex64_t zbetaloc = (flagXP) ? 0.  : zbeta;
                        
                        // Accumulate in extended precision
                        // - every freq tiles
                        // - if last tile
                        flagXP = ( ((k+1)%freq==0) || (k+1==A.nt) );

                        if(flagXP){    
                            
                            core_omp_zcgemm_extended(
                                transa, transb,
                                mvcm, nvcn, nvak,
                                alpha, A(m, k), ldam,
                                       B(k, n), ldbk,
                                zbetaloc, D(m, n), ldcm,
                                C(m, n), ldcm, 
                                sequence, request);

                        }
                        else {

                            #ifdef COMPLEX
                            core_omp_cgemm(
                                transa, transb,
                                mvcm, nvcn, nvak,
                                alpha, A(m, k), ldam,
                                       B(k, n), ldbk,
                                cbetaloc, C(m, n), ldcm,
                                sequence, request);
                            #else
                            core_omp_sgemm(
                                transa, transb,
                                mvcm, nvcn, nvak,
                                alpha, A(m, k), ldam,
                                       B(k, n), ldbk,
                                cbetaloc, C(m, n), ldcm,
                                sequence, request);
                            #endif

                        }
                        
                    } // end loop over k

                }
                //=====================================
                // PlasmaNoTrans / Plasma[_Conj]Trans
                //=====================================
                else {
                    int ldbn = plasma_tile_mmain(B, n);
                    for (int k = 0; k < A.nt; k++) {
                        int nvak = plasma_tile_nview(A, k);
                        plasma_complex64_t zbeta = k == 0 ? beta : 1.0;

                        // If extended precision was used in previous sum
                        // then start with a fresh accumulator by setting beta=0
                        plasma_complex32_t cbetaloc = (flagXP) ? 0.f : (float)zbeta;
                        plasma_complex64_t zbetaloc = (flagXP) ? 0.  : zbeta;
                        
                        // Accumulate in extended precision when
                        // - every freq tiles
                        // - if last tile
                        flagXP = ( ((k+1)%freq==0) || (k+1==A.nt) );

                        if(flagXP){  

                            core_omp_zcgemm_extended(
                                transa, transb,
                                mvcm, nvcn, nvak,
                                alpha, A(m, k), ldam,
                                       B(n, k), ldbn,
                                zbetaloc, D(m, n), ldcm,
                                C(m, n), ldcm, 
                                sequence, request);

                        }
                        else {

                            #ifdef COMPLEX
                            core_omp_cgemm(
                                transa, transb,
                                mvcm, nvcn, nvak,
                                alpha, A(m, k), ldam,
                                       B(k, n), ldbn,
                                cbetaloc, C(m, n), ldcm,
                                sequence, request);
                            #else
                            core_omp_sgemm(
                                transa, transb,
                                mvcm, nvcn, nvak,
                                alpha, A(m, k), ldam,
                                       B(n, k), ldbn,
                                cbetaloc, C(m, n), ldcm,
                                sequence, request);
                            #endif

                        }
                    }
                }
            }
            else {
                //=====================================
                // Plasma[_Conj]Trans / PlasmaNoTrans
                //=====================================
                if (transb == PlasmaNoTrans) {
                    for (int k = 0; k < A.mt; k++) {
                        int mvak = plasma_tile_mview(A, k);
                        int ldak = plasma_tile_mmain(A, k);
                        int ldbk = plasma_tile_mmain(B, k);
                        plasma_complex64_t zbeta = k == 0 ? beta : 1.0;

                        // If extended precision was used in previous sum
                        // then start with a fresh accumulator by setting beta=0
                        plasma_complex32_t cbetaloc = (flagXP) ? 0.f : (float)zbeta;
                        plasma_complex64_t zbetaloc = (flagXP) ? 0.  : zbeta;
                        
                        // Accumulate in extended precision when
                        // - every freq tiles
                        // - if last tile
                        flagXP = ( ((k+1)%freq==0) || (k+1==A.mt) );

                        if(flagXP){  

                            core_omp_zcgemm_extended(
                                transa, transb,
                                mvcm, nvcn, mvak,
                                alpha, A(k, m), ldak,
                                       B(k, n), ldbk,
                                zbetaloc, D(m, n), ldcm,
                                C(m, n), ldcm, 
                                sequence, request);

                        }
                        else {

                            #ifdef COMPLEX
                            core_omp_cgemm(
                                transa, transb,
                                mvcm, nvcn, mvak,
                                alpha, A(k, m), ldak,
                                       B(k, n), ldbk,
                                cbetaloc, C(m, n), ldcm,
                                sequence, request);
                            #else
                            core_omp_sgemm(
                                transa, transb,
                                mvcm, nvcn, mvak,
                                alpha, A(k, m), ldak,
                                       B(k, n), ldbk,
                                cbetaloc, C(m, n), ldcm,
                                sequence, request);
                            #endif

                        }

                    }
                }
                //==========================================
                // Plasma[_Conj]Trans / Plasma[_Conj]Trans
                //==========================================
                else {
                    int ldbn = plasma_tile_mmain(B, n);
                    for (int k = 0; k < A.mt; k++) {
                        int mvak = plasma_tile_mview(A, k);
                        int ldak = plasma_tile_mmain(A, k);
                        plasma_complex64_t zbeta = k == 0 ? beta : 1.0;

                        // If extended precision was used in previous sum
                        // then start with a fresh accumulator by setting beta=0
                        plasma_complex32_t cbetaloc = (flagXP) ? 0.f : (float)zbeta;
                        plasma_complex64_t zbetaloc = (flagXP) ? 0.  : zbeta;
                        
                        // Accumulate in extended precision when
                        // - every freq tiles
                        // - if last tile
                        flagXP = ( ((k+1)%freq==0) || (k+1==A.mt) );

                        if(flagXP){  

                            core_omp_zcgemm_extended(
                                transa, transb,
                                mvcm, nvcn, mvak,
                                alpha, A(k, m), ldak,
                                       B(n, k), ldbn,
                                zbetaloc, D(m, n), ldcm,
                                C(m, n), ldcm, 
                                sequence, request);

                        }
                        else {

                            #ifdef COMPLEX
                            core_omp_cgemm(
                                transa, transb,
                                mvcm, nvcn, mvak,
                                alpha, A(k, m), ldak,
                                       B(n, k), ldbn,
                                cbetaloc, C(m, n), ldcm,
                                sequence, request);
                            #else
                            core_omp_sgemm(
                                transa, transb,
                                mvcm, nvcn, mvak,
                                alpha, A(k, m), ldak,
                                       B(n, k), ldbn,
                                cbetaloc, C(m, n), ldcm,
                                sequence, request);
                            #endif

                        }

                    }
                }
            }
        }
    }
}

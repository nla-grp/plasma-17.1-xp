/**
 *
 * @file
 *
 *  PLASMA is a software package provided by:
 *  University of Tennessee, US,
 *  University of Manchester, UK.
 *
 * @precisions mixed zc -> ds
 *
 **/

#include "plasma_async.h"
#include "plasma_context.h"
#include "plasma_descriptor.h"
#include "plasma_internal.h"
#include "plasma_types.h"
#include "plasma_workspace.h"
#include "core_blas.h"

#define A(m, n) (plasma_complex32_t*)plasma_tile_addr(A, m, n)
#define Az(m, n) (plasma_complex64_t*)plasma_tile_addr(Az, m, n)

/******************************************************************************/
void plasma_pzcgetrf_extended(plasma_desc_t A, int *ipiv, plasma_desc_t Az,
                    plasma_sequence_t *sequence, plasma_request_t *request)
{
    // Return if failed sequence.
    if (sequence->status != PlasmaSuccess)
        return;

    // Read parameters from the context.
    plasma_context_t *plasma = plasma_context_self();
    int ib = plasma->ib;
    int num_panel_threads = plasma->num_panel_threads;
    plasma_barrier_t *barrier = &plasma->barrier;

    for (int k = 0; k < imin(A.mt, A.nt); k++) {

        // Single precision pointers
        plasma_complex32_t *a00, *a20;
        a00 = A(k, k);
        a20 = A(A.mt-1, k);

        // Double precision pointers
        plasma_complex64_t *z00, *z20;
        z00 = Az(k, k);
        z20 = Az(A.mt-1, k);


        // Sizes
        int ma00k = (A.mt-k-1)*A.mb;
        int na00k = plasma_tile_nmain(A, k);
        int lda20 = plasma_tile_mmain(A, A.mt-1);

        int nvak = plasma_tile_nview(A, k);
        int mvak = plasma_tile_mview(A, k);
        int ldak = plasma_tile_mmain(A, k);

        // panel factorization
        // ... done in single precision
        #pragma omp task depend(inout:a00[0:ma00k*na00k]) \
                         depend(inout:a20[0:lda20*nvak]) \
                         depend(out:ipiv[k*A.mb:mvak]) /*\
                         priority(1) */
        {
            if (sequence->status == PlasmaSuccess) {
                for (int rank = 0; rank < num_panel_threads; rank++) {
                    #pragma omp task // priority(1)
                    {

                        // for (int i = 0; i < A.m*A.n; i++) 
                        //     printf("%e ", (A(0,0))[i]);
                        // printf("\n");
                        
                        // for (int i = 0; i < A.m*A.n; i++) 
                        //     printf("%e ", (Az(0,0))[i]);
                        // printf("\n");
                        

                        plasma_desc_t view =
                            plasma_desc_view(A,
                                             k*A.mb, k*A.nb,
                                             A.m-k*A.mb, nvak);

                        int info = core_cgetrf(view, &ipiv[k*A.mb], ib,
                                               rank, num_panel_threads,
                                               barrier);
                        if (info != 0)
                            plasma_request_fail(sequence, request, k*A.mb+info);
                    }
                }
            }
            #pragma omp taskwait

            for (int i = k*A.mb+1; i <= imin(A.m, k*A.mb+nvak); i++)
                ipiv[i-1] += k*A.mb;
        }
        // update
        // ... done in single precision
        // ... accumulated in a double precision array
        // ... convert only the necessary bit back to single precision
        for (int n = k+1; n < A.nt; n++) {

            // Single precision blocks
            plasma_complex32_t *a01, *a11, *a21;
            a01 = A(k, n);
            a11 = A(k+1, n);
            a21 = A(A.mt-1, n);

            int ma11k = (A.mt-k-2)*A.mb;
            int na11n = plasma_tile_nmain(A, n);
            int lda21 = plasma_tile_mmain(A, A.mt-1);

            int nvan = plasma_tile_nview(A, n);

            #pragma omp task depend(in:a00[0:ma00k*na00k]) \
                             depend(in:a20[0:lda20*nvak]) \
                             depend(in:ipiv[k*A.mb:mvak]) \
                             depend(inout:a01[0:ldak*nvan]) \
                             depend(inout:a11[0:ma11k*na11n]) \
                             depend(inout:a21[0:lda21*nvan]) /*\
                             priority(n == k+1) */
            {
                if (sequence->status == PlasmaSuccess) {
                    // laswp
                    int k1 = k*A.mb+1;
                    int k2 = imin(k*A.mb+A.mb, A.m);
                    plasma_desc_t view =
                        plasma_desc_view(A, 0, n*A.nb, A.m, nvan);
                    core_claswp(PlasmaRowwise, view, k1, k2, ipiv, 1);

                    plasma_desc_t view_z =
                        plasma_desc_view(Az, 0, n*A.nb, A.m, nvan);
                    core_zlaswp(PlasmaRowwise, view_z, k1, k2, ipiv, 1);


                    // trsm
                    core_ctrsm(PlasmaLeft, PlasmaLower,
                               PlasmaNoTrans, PlasmaUnit,
                               mvak, nvan,
                               1.0, A(k, k), ldak,
                                    A(k, n), ldak);

                    // gemm
                    for (int m = k+1; m < A.mt; m++) {
                        int mvam = plasma_tile_mview(A, m);
                        int ldam = plasma_tile_mmain(A, m);

                        #pragma omp task // priority(n == k+1)
                        {
                        
                            // core_cgemm(
                            //     PlasmaNoTrans, PlasmaNoTrans,
                            //     mvam, nvan, A.nb,
                            //     -1.0, A(m, k), ldam,
                            //           A(k, n), ldak,
                            //     1.0,  A(m, n), ldam);


// THERE IS STILL A PROBLEM IN COMPLEX ARITH. Probs some constant not interpreted well. And problem should also be in gemm

                            // Here we store the result of the single precision local mat-mult
                            plasma_complex32_t *Amn =
                              (plasma_complex32_t*)malloc((size_t)ldam*nvan*sizeof(plasma_complex32_t));
                            assert(Amn != NULL);

                            // Perform local mat mult in single precision
#ifdef COMPLEX
                            core_cgemm(PlasmaNoTrans, PlasmaNoTrans,
                                       mvam, nvan, A.nb,
                                       -1.0, A(m, k),  ldam,
                                             A(k, n),  ldak,
                                        0.0, Amn,      ldam);
#else
                            core_sgemm(PlasmaNoTrans, PlasmaNoTrans,
                                       mvam, nvan, A.nb,
                                       -1.0, A(m, k),  ldam,
                                             A(k, n),  ldak,
                                        0.0, Amn,      ldam);
#endif        

                            // Here we store the result of the single precision local mat-mult
                            plasma_complex64_t *zAmn =
                              (plasma_complex64_t*)malloc((size_t)ldam*nvan*sizeof(plasma_complex64_t));
                            assert(zAmn != NULL);

                            // Convert local result to extended precision
                            core_clag2z(mvam, nvan, Amn, ldam, zAmn, ldam);

                            free(Amn);

                            // Then accumulate in double precision
                            core_zgeadd(PlasmaNoTrans, mvam, nvan, 1.0, zAmn, ldam, 1.0, Az(m, n), ldam);

                            free(zAmn);


                            // Convert the tiles that will be factored 
                            // or used as rhs in trsm in next iterate.
                            // ... if that works then maybe wee only need to store a vert + horiz panel 
                            if(n==k+1 /*for next getrf*/ || m==k+1 /*for next trsm*/)
                            { 

                        // THIS DOES NOT SEEM ENOUGH ... BUT IF WE APPLY CONVERSION TO EACH TILE
                        // THEN WE CANT GET MUCH BETTER ACCURACY THAN SINGLE PREC LU ...


                        // #pragma omp taskwait
                        // printf("A(m,n) ");
                        // for (int i = 0; i < mvam; i++) 
                        //     for (int j = 0; j < nvan; j++) 
                        //         printf("%e ", (A(m,n))[i+j*ldam]);
                        // printf("\n");

                        // #pragma omp taskwait
                        // printf("Az(m,n) ");
                        // for (int i = 0; i < mvam; i++) 
                        //     for (int j = 0; j < nvan; j++) 
                        //         printf("%e ", (Az(m,n))[i+j*ldam]);
                        // printf("\n");

                                // printf("Convert tile (m=%d,n=%d)\n",m,n);
                                core_zlag2c(mvam, nvan, Az(m, n), ldam, A(m, n), ldam);
        
                        // #pragma omp taskwait
                        // printf("conv A(m,n) ");
                        // for (int i = 0; i < mvam; i++) 
                        //     for (int j = 0; j < nvan; j++) 
                        //         printf("%e ", (A(m,n))[i+j*ldam]);
                        // printf("\n");


                            }

                            #pragma omp taskwait
                            printf("A(m=%d,n=%d) ",m,n);
                            for (int i = 0; i < mvam; i++) 
                                for (int j = 0; j < nvan; j++) 
                                    printf("%e ", (A(m,n))[i+j*ldam]);
                            printf("\n");


                        }
                    }
                }
                #pragma omp taskwait
            }
        }
    }
    // pivoting to the left
    for (int k = 1; k < imin(A.mt, A.nt); k++) {
        plasma_complex32_t *akk;
        akk = A(k, k);
        int makk = (A.mt-k-1)*A.mb;
        int nakk = plasma_tile_nmain(A, k);

        #pragma omp task depend(in:ipiv[(imin(A.mt, A.nt)-1)*A.mb]) \
                         depend(inout:akk[0:makk*nakk])
        {
            if (sequence->status == PlasmaSuccess) {
                plasma_desc_t view =
                    plasma_desc_view(A, 0, (k-1)*A.nb, A.m, A.nb);
                int k1 = k*A.mb+1;
                int k2 = imin(A.m, A.n);
                core_claswp(PlasmaRowwise, view, k1, k2, ipiv, 1);

                plasma_desc_t view_z =
                    plasma_desc_view(Az, 0, (k-1)*A.nb, A.m, A.nb);
                core_zlaswp(PlasmaRowwise, view_z, k1, k2, ipiv, 1);
            }
        }
    }
}

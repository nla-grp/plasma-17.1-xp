#!/bin/bash


#OMP_NUM_THREADS=1 ./test/test sgemm --m=1024 --n=1024 --k=524288 --nb=128,192,256,320,384,448,512 --alpha=1.0 --beta=0.0 --xp=n


# # Kebnekaise
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemm-xp.sh n 28
# A correct implementation for extended precision woth freq
sbatch -A SNIC2018-3-624 --time 04:00:00 --exclusive -Cbroadwell ./gemm-xp.sh c 28 1
sbatch -A SNIC2018-3-624 --time 04:00:00 --exclusive -Cbroadwell ./gemm-xp.sh c 28 2
sbatch -A SNIC2018-3-624 --time 04:00:00 --exclusive -Cbroadwell ./gemm-xp.sh c 28 4
sbatch -A SNIC2018-3-624 --time 04:00:00 --exclusive -Cbroadwell ./gemm-xp.sh c 28 8
sbatch -A SNIC2018-3-624 --time 04:00:00 --exclusive -Cbroadwell ./gemm-xp.sh c 28 16

# A not so correct version of compensation with freq
## try freq nt/2 hen increase each 
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemm-xp.sh c 28 2
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemm-xp.sh c 28 4
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemm-xp.sh c 28 8
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemm-xp.sh c 28 16
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemm-xp.sh e 28

## Similar to gemm but with size n close to 1 and other sizes larger
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemv-xp.sh n 28 0
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemv-xp.sh c 28 1
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemv-xp.sh c 28 2
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemv-xp.sh c 28 4
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemv-xp.sh c 28 8
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemv-xp.sh c 28 16
# sbatch -A SNIC2018-3-624 --time 02:00:00 --exclusive -Cbroadwell ./gemv-xp.sh e 28


# # Saturn
# # sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./gemm-xp.sh n 1
# sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./gemm-xp.sh n 28
# # sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./gemm-xp.sh c 1
# sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./gemm-xp.sh c 28
# # sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./gemm-xp.sh e 1
# sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./gemm-xp.sh e 28



# srun -p exclusive -Calembert --time 00:10:00 -N 1 ./test/test zcgemm --m=1024 --n=1024 --k=131072 --nb=256 --beta=0.0 

# srun -A SNIC2018-3-624 -Cbroadwell --time 00:10:00 -N 1 ./test/test zcgemm --m=1024 --n=1024 --k=131072 --nb=256 --beta=0.0 
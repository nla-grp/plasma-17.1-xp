#!/bin/bash

#SBATCH -J gemv-xp
#SBATCH -o job/plasma17-xp-gemv.%j.out

# xp=n,c,e
xp=$1
ncores=$2


##
## Run for fixed nb and varying sizes
##
# k_ar=(1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576) 
k_ar=(1024 2048 4096 8192 16384 32768) 
# nb_ar=(64 128 256)
# k_ar=(1024 2048 4096 8192 16384 32768 65536 131072) 
nb_ar=(128)

n=16


k_list=""
for k in "${k_ar[@]}"
do
    if [[ "${k_list}" = "" ]]; then
        k_list="${k}"
    else
        k_list="${k_list},${k}"
    fi
done

echo -e "Run dsgemm on ${ncores} cores, with nb=${nb}, n=${n} and m=k={${k_list}}..."

for r in {0..10..1}
do
    for nb in "${nb_ar[@]}"
    do

        filename="dat/dsgemm-xp${xp}-nc${ncores}-n${n}-nb${nb}-vs-mk-run${r}.dat"

        # iargs="--m=${nb} --n=${nb} --k=${k_list}"
        iargs="--m=${k_list} --n=${n} --k=${k_list}"

        iargs="${iargs} --nb=${nb} --alpha=1.0 --beta=0.0 --xp=${xp}"
        echo -e "OMP_NUM_THREADS=${ncores} ../test/test dsgemm ${iargs} &> ${filename}"
        OMP_NUM_THREADS=${ncores} ../test/test dsgemm ${iargs} &> ${filename}

    done
done

# ##
# ## Run for fixed size and varying nb
# ##
# # k=262144
# k=16384
# # mn=8192
# # mn=4096
# mn=16384
# nb_ar=(128 256 384 512 768 1024 2048 4096)
# # nb_ar=(192 224 256 320 384 448 512 576 640)
# # nb_ar=(128 192 256 320 384 448 512)
# # "128,192,256,320,384,448,512"

# nb_list=""
# for nb in "${nb_ar[@]}"
# do
#     if [[ "${nb_list}" = "" ]]; then
#         nb_list="${nb}"
#     else
#         nb_list="${nb_list},${nb}"
#     fi
# done

# echo -e "Run dsgemm on ${ncores} cores and k=$k..."

# for r in {0..5..1}
# do

#     filename="dat/dsgemm-xp${xp}-nc${ncores}-mn${mn}-k${k}-vs-nb-run${r}.dat"

#     iargs="--m=${mn} --n=${mn} --k=${k}"

#     iargs="${iargs} --nb=${nb_list} --alpha=1.0 --beta=0.0 --xp=${xp}"
#     echo -e "OMP_NUM_THREADS=${ncores} ../test/test dsgemm ${iargs} &> ${filename}"
#     OMP_NUM_THREADS=${ncores} ../test/test dsgemm ${iargs} &> ${filename}

# done

#!/bin/bash

#SBATCH -J getrf-xp-nb
#SBATCH -o job/plasma17-xp-getrf-nb.%j.out

# xp=n,c,e
xp=$1
ncores=$2
cond=$3

##
## Run for fixed size and varying nb
##
# k=16384
k=8192
nb_ar=(16 32 64 96 128 192 256)
ib_ar=(4 8 16 24 32 48 64)

nb_list=""
for nb in "${nb_ar[@]}"
do
    if [[ "${nb_list}" = "" ]]; then
        nb_list="${nb}"
    else
        nb_list="${nb_list},${nb}"
    fi
done

ib_list=""
for ib in "${ib_ar[@]}"
do
    if [[ "${ib_list}" = "" ]]; then
        ib_list="${ib}"
    else
        ib_list="${ib_list},${ib}"
    fi
done

echo -e "Run dsgetrf on ${ncores} cores and m=n=$k..."

for r in {0..5..1}
do
    filename="dat/dsgetrf-xp${xp}-cond${cond}-nc${ncores}-mnk${k}-vs-nb-run${r}.dat"

    iargs="--m=${k} --n=${k}"
    iargs="${iargs} --nb=${nb_list} --ib=${ib_list} --xp=${xp} --cond=${cond}"
    echo -e "OMP_NUM_THREADS=${ncores} ../test/test dsgetrf ${iargs} &> ${filename}"
    OMP_NUM_THREADS=${ncores} ../test/test dsgetrf ${iargs} &> ${filename}
done

#!/bin/bash


#OMP_NUM_THREADS=1 ./test/test sgetrf --m=1024 --n=1024 --k=524288 --nb=128,192,256,320,384,448,512 --alpha=1.0 --beta=0.0 --xp=n


# # Kebnekaise
# sbatch -A SNIC2018-3-624 --exclusive -Cbroadwell ./getrf-xp.sh n 1
# sbatch -A SNIC2018-3-624 --time 01:00:00 --exclusive -Cbroadwell ./getrf-xp.sh n 28 1e0
# sbatch -A SNIC2018-3-624 --time 01:00:00 --exclusive -Cbroadwell ./getrf-xp.sh c 28 1e0
# # sbatch -A SNIC2018-3-624 --time 01:00:00 --exclusive -Cbroadwell ./getrf-xp.sh e 28 1e0

# sbatch -A SNIC2018-3-624 --time 04:00:00 --exclusive -Cbroadwell ./getrf-xp.sh n 28 0
# sbatch -A SNIC2018-3-624 --time 04:00:00 --exclusive -Cbroadwell ./getrf-xp.sh c 28 0

sbatch -A SNIC2018-3-624 --time 04:00:00 --exclusive -Cbroadwell ./getrf-xp.sh n 28 1e4
sbatch -A SNIC2018-3-624 --time 04:00:00 --exclusive -Cbroadwell ./getrf-xp.sh c 28 1e4

# sbatch -A SNIC2018-3-624 --time 01:00:00 --exclusive -Cbroadwell ./getrf-xp.sh n 28 1e8
# sbatch -A SNIC2018-3-624 --time 01:00:00 --exclusive -Cbroadwell ./getrf-xp.sh c 28 1e8

# vs nb

# sbatch -A SNIC2018-3-624 --time 01:00:00 --exclusive -Cbroadwell ./getrf-xp-vs-nb.sh n 28 1e0
# sbatch -A SNIC2018-3-624 --time 01:00:00 --exclusive -Cbroadwell ./getrf-xp-vs-nb.sh c 28 1e0

# sbatch -A SNIC2018-3-624 --time 03:00:00 --exclusive -Cbroadwell ./getrf-xp-vs-nb.sh n 28 0
# sbatch -A SNIC2018-3-624 --time 03:00:00 --exclusive -Cbroadwell ./getrf-xp-vs-nb.sh c 28 0

# sbatch -A SNIC2018-3-624 --time 03:00:00 --exclusive -Cbroadwell ./getrf-xp-vs-nb.sh n 28 1e4
# sbatch -A SNIC2018-3-624 --time 03:00:00 --exclusive -Cbroadwell ./getrf-xp-vs-nb.sh c 28 1e4

# sbatch -A SNIC2018-3-624 --time 01:00:00 --exclusive -Cbroadwell ./getrf-xp-vs-nb.sh n 28 1e8
# sbatch -A SNIC2018-3-624 --time 01:00:00 --exclusive -Cbroadwell ./getrf-xp-vs-nb.sh c 28 1e8


# # Saturn
# # sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./getrf-xp.sh n 1
# sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./getrf-xp.sh n 28
# # sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./getrf-xp.sh c 1
# sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./getrf-xp.sh c 28
# # sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./getrf-xp.sh e 1
# sbatch -p exclusive -Calembert --time 00:30:00 -N 1 ./getrf-xp.sh e 28



# srun -p exclusive -Calembert --time 00:10:00 -N 1 ./test/test zcgetrf --m=1024 --n=1024 --k=131072 --nb=256 

# srun -A SNIC2018-3-624 -Cbroadwell --time 00:10:00 -N 1 ./test/test zcgetrf --m=1024 --n=1024 --k=1024 --nb=256 
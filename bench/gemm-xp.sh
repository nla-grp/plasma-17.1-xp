#!/bin/bash

#SBATCH -J gemm-xp
#SBATCH -o job/plasma17-xp-gemm.%j.out

# xp=n,c,e
xp=$1
ncores=$2
freq=$3

##
## Run for fixed nb and varying sizes
##
# k_ar=(1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152) 
# k_ar=(1024 2048 4096 8192 16384 32768 65536 131072 262144 524288) # perf starts decreasing after 262144
k_ar=(1024 2048 4096 8192 16384 32768 65536 131072 262144) # perf starts decreasing after 262144
# nb_ar=(64 128 256)
# k_ar=(1024 2048 4096 8192 16384 32768 65536 131072) 
nb_ar=(256)

mn=4096


k_list=""
for k in "${k_ar[@]}"
do
    if [[ "${k_list}" = "" ]]; then
        k_list="${k}"
    else
        k_list="${k_list},${k}"
    fi
done

echo -e "Run dsgemm on ${ncores} cores, with nb=${nb} and k={${k_list}}... freq=${freq}"

for r in {0..10..1}
do
    for nb in "${nb_ar[@]}"
    do

        iargs="--m=${mn} --n=${mn} --k=${k_list}"

        iargs="${iargs} --nb=$nb --alpha=1.0 --beta=0.0 --xp=${xp} --freq=${freq}"

        if [[ "${xp}" = "n" ]]; then
            filename="dat/dsgemm-xp${xp}-nc${ncores}-mn${mn}-nb${nb}-vs-k-run${r}.dat"
        else
            # filename="dat/dsgemm-xp${xp}-freq${freq}inc-nc${ncores}-mn${mn}-nb${nb}-vs-k-run${r}.dat"
            # filename="dat/dsgemm-xp${xp}-freq${freq}kp1-nc${ncores}-mn${mn}-nb${nb}-vs-k-run${r}.dat"
            filename="dat/dsgemm-xp${xp}-freq${freq}-bigtask2-nc${ncores}-mn${mn}-nb${nb}-vs-k-run${r}.dat"
        fi


        echo -e "OMP_NUM_THREADS=${ncores} ../test/test dsgemm ${iargs} &> ${filename}"
        OMP_NUM_THREADS=${ncores} ../test/test dsgemm ${iargs} &> ${filename}

    done
done

# ##
# ## Run for fixed size and varying nb
# ##
# # k=262144
# k=16384
# # mn=8192
# mn=4096
# # mn=16384
# # without frequency
# # nb_ar=(128 256 384 512 768 1024 2048 4096)
# # with frequency
# nb_ar=(256 384 512 768)

# nb_list=""
# for nb in "${nb_ar[@]}"
# do
#     if [[ "${nb_list}" = "" ]]; then
#         nb_list="${nb}"
#     else
#         nb_list="${nb_list},${nb}"
#     fi
# done

# echo -e "Run dsgemm on ${ncores} cores and k=$k..."

# for r in {0..5..1}
# do
#     if [[ "${xp}" = "n" ]]; then
#         filename="dat/dsgemm-xp${xp}-nc${ncores}-mn${mn}-k${k}-vs-nb-run${r}.dat"
#     else
#         filename="dat/dsgemm-xp${xp}-freq${freq}-nc${ncores}-mn${mn}-k${k}-vs-nb-run${r}.dat"
#     fi

#     iargs="--m=${mn} --n=${mn} --k=${k}"

#     iargs="${iargs} --nb=${nb_list} --alpha=1.0 --beta=0.0 --xp=${xp} --freq=${freq}"
#     echo -e "OMP_NUM_THREADS=${ncores} ../test/test dsgemm ${iargs} &> ${filename}"
#     OMP_NUM_THREADS=${ncores} ../test/test dsgemm ${iargs} &> ${filename}

# done

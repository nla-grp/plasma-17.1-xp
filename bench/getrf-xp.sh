#!/bin/bash

#SBATCH -J getrf-xp
#SBATCH -o job/plasma17-xp-getrf.%j.out

# xp=n,c,e
xp=$1
ncores=$2
cond=$3

##
## Run for fixed nb and varying sizes
##
# k_ar=(1024 2048 4096 8192 16384 32768 65536 131072 262144) 
# k_ar=(512 1024 2048 4096 8192 16384 32768 65536) 
k_ar=(256 512 1024 2048 4096 8192 16384 32768) 
# nb_ar=(64 128 256)
nb_ar=(128)

# mn=4096

k_list=""
for k in "${k_ar[@]}"
do
    if [[ "${k_list}" = "" ]]; then
        k_list="${k}"
    else
        k_list="${k_list},${k}"
    fi
done

echo -e "Run dsgetrf on ${ncores} cores, with nb=${nb} and m=n={${k_list}}..."

for r in {0..5..1}
do
    for nb in "${nb_ar[@]}"
    do

        ib=$(( $nb / 4 ))

        filename="dat/dsgetrf-xp${xp}-cond${cond}-nc${ncores}-nb${nb}-vs-mnk-run${r}.dat"

        # iargs="--m=${nb} --n=${nb} --k=${k_list}"
        # iargs="--m=${mn} --n=${mn} --k=${k_list}"
        # Square matrix!
        iargs="--m=${k_list} --n=${k_list}"

        iargs="${iargs} --nb=$nb --ib=$ib --xp=${xp} --cond=${cond}"
        echo -e "OMP_NUM_THREADS=${ncores} ../test/test dsgetrf ${iargs} &> ${filename}"
        OMP_NUM_THREADS=${ncores} ../test/test dsgetrf ${iargs} &> ${filename}

    done
done

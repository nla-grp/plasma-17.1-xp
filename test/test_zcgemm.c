/**
 *
 * @file
 *
 *  PLASMA is a software package provided by:
 *  University of Tennessee, US,
 *  University of Manchester, UK.
 *
 * @precisions mixed zc -> ds
 *
 **/
#include "test.h"
#include "flops.h"
#include "core_lapack.h"
#include "plasma.h"

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <omp.h>

#define COMPLEX

/***************************************************************************//**
 *
 * @brief Tests ZCGEMM.
 *
 * @param[in]  param - array of parameters
 * @param[out] info  - string of column labels or column values; length InfoLen
 *
 * If param is NULL and info is NULL,     print usage and return.
 * If param is NULL and info is non-NULL, set info to column labels and return.
 * If param is non-NULL and info is non-NULL, set info to column values
 * and run test.
 ******************************************************************************/
void test_zcgemm(param_value_t param[], char *info)
{

    //================================================================
    // Command line examples
    //================================================================
    
    // ./test zcgemm --m=256 --n=256 --k=1024,2048,4096,8192,16384,32768,65536,131072,262144,524288,1048576,2097152 --nb=256 --beta=1.0 
    // ./test zcgemm --m=256 --n=256 --k=131072 --nb=256 --beta=0.0 --xp=n --freq=2

    //================================================================
    // Print usage info or return column labels or values.
    //================================================================
    if (param == NULL) {
        if (info == NULL) {
            // Print usage info.
            print_usage(PARAM_TRANSA);
            print_usage(PARAM_TRANSB);
            print_usage(PARAM_M);
            print_usage(PARAM_N);
            print_usage(PARAM_K);
            print_usage(PARAM_ALPHA);
            print_usage(PARAM_BETA);
            print_usage(PARAM_PADA);
            print_usage(PARAM_PADB);
            print_usage(PARAM_PADC);
            print_usage(PARAM_NB);
            print_usage(PARAM_XP);
            print_usage(PARAM_DIST);
            print_usage(PARAM_FREQ);
        }
        else {
            // Return column labels.
            snprintf(info, InfoLen,
                     // "%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s",
                     "%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s",
                     InfoSpacing, "XP",
                     InfoSpacing, "FREQ",
                     InfoSpacing, "TransA",
                     InfoSpacing, "TransB",
                     InfoSpacing, "M",
                     InfoSpacing, "N",
                     InfoSpacing, "K",
                     InfoSpacing, "alpha",
                     InfoSpacing, "beta",
                     // InfoSpacing, "PadA",
                     // InfoSpacing, "PadB",
                     // InfoSpacing, "PadC",
                     InfoSpacing, "NB"
                     );
        }
        return;
    }
    // Return column values.
    snprintf(info, InfoLen,
             // "%*c %*c %*c %*d %*d %*d %*.4f %*.4f %*d %*d %*d %*d",
             "%*c %*d %*c %*c %*d %*d %*d %*.4f %*.4f %*d",
             InfoSpacing, param[PARAM_XP].c,
             InfoSpacing, param[PARAM_FREQ].c,
             InfoSpacing, param[PARAM_TRANSA].c,
             InfoSpacing, param[PARAM_TRANSB].c,
             InfoSpacing, param[PARAM_M].i,
             InfoSpacing, param[PARAM_N].i,
             InfoSpacing, param[PARAM_K].i,
             InfoSpacing, creal(param[PARAM_ALPHA].z),
             InfoSpacing, creal(param[PARAM_BETA].z),
             // InfoSpacing, param[PARAM_PADA].i,
             // InfoSpacing, param[PARAM_PADB].i,
             // InfoSpacing, param[PARAM_PADC].i,
             InfoSpacing, param[PARAM_NB].i);

    //================================================================
    // Set parameters.
    //================================================================
    int freq = param[PARAM_FREQ].i;

    plasma_enum_t transa = plasma_trans_const(param[PARAM_TRANSA].c);
    plasma_enum_t transb = plasma_trans_const(param[PARAM_TRANSB].c);

    int m = param[PARAM_M].i;
    int n = param[PARAM_N].i;
    int k = param[PARAM_K].i;

    int Am, An;
    int Bm, Bn;
    int Cm, Cn;

    if (transa == PlasmaNoTrans) {
        Am = m;
        An = k;
    }
    else {
        Am = k;
        An = m;
    }
    if (transb == PlasmaNoTrans) {
        Bm = k;
        Bn = n;
    }
    else {
        Bm = n;
        Bn = k;
    }
    Cm = m;
    Cn = n;

    int lda = imax(1, Am + param[PARAM_PADA].i);
    int ldb = imax(1, Bm + param[PARAM_PADB].i);
    int ldc = imax(1, Cm + param[PARAM_PADC].i);

    int test = param[PARAM_TEST].c == 'y';
    float eps = LAPACKE_slamch('E');

#ifdef COMPLEX
    plasma_complex32_t alpha = param[PARAM_ALPHA].z;
    plasma_complex32_t beta  = param[PARAM_BETA].z;
#else
    float alpha = creal(param[PARAM_ALPHA].z);
    float beta  = creal(param[PARAM_BETA].z);
#endif

    //================================================================
    // Set tuning parameters.
    //================================================================
    plasma_set(PlasmaNb, param[PARAM_NB].i);

    //================================================================
    // Allocate and initialize arrays.
    //================================================================
    plasma_complex32_t *A =
        (plasma_complex32_t*)malloc((size_t)lda*An*sizeof(plasma_complex32_t));
    assert(A != NULL);

    plasma_complex32_t *B =
        (plasma_complex32_t*)malloc((size_t)ldb*Bn*sizeof(plasma_complex32_t));
    assert(B != NULL);

    plasma_complex32_t *C =
        (plasma_complex32_t*)malloc((size_t)ldc*Cn*sizeof(plasma_complex32_t));
    assert(C != NULL);

    int seed[] = {0, 0, 0, 1};
    lapack_int retval;

    int idist = 1;
    if(param[PARAM_DIST].c=='n')
        idist = 2;


    retval = LAPACKE_clarnv(idist, seed, (size_t)lda*An, A);
    assert(retval == 0);

    retval = LAPACKE_clarnv(idist, seed, (size_t)ldb*Bn, B);
    assert(retval == 0);

    retval = LAPACKE_clarnv(idist, seed, (size_t)ldc*Cn, C);
    assert(retval == 0);


    // Save initial matrix C in Cref
    plasma_complex32_t *Cref =
        (plasma_complex32_t*)malloc((size_t)ldc*Cn*sizeof(plasma_complex32_t));
    assert(Cref != NULL);

#ifdef COMPLEX
    plasma_clacpy(PlasmaGeneral,Cm,Cn,C,ldc,Cref,ldc);
#else
    plasma_slacpy(PlasmaGeneral,Cm,Cn,C,ldc,Cref,ldc);
#endif

    float work[1];

    float Cnorm = LAPACKE_clange_work(
        LAPACK_COL_MAJOR, 'F', Cm, Cn, C, ldc, work);

    //================================================================
    // Run and time PLASMA.
    //================================================================
    plasma_time_t start = omp_get_wtime();

    if(param[PARAM_XP].c=='c')
    {
#ifdef COMPLEX
        plasma_cgemm_compensated(
            transa, transb,
            m, n, k,
            alpha, A, lda,
                   B, ldb,
             beta, C, ldc, freq);
#else
        plasma_sgemm_compensated(
            transa, transb,
            m, n, k,
            alpha, A, lda,
                   B, ldb,
             beta, C, ldc, freq);
#endif        
    }
    else if(param[PARAM_XP].c=='e')
    {

        plasma_zcgemm_extended(
            transa, transb,
            m, n, k,
            alpha, A, lda,
                   B, ldb,
            beta, C, ldc, freq);
    }
    else
    {
#ifdef COMPLEX
        plasma_cgemm(
            transa, transb,
            m, n, k,
            alpha, A, lda,
                   B, ldb,
             beta, C, ldc);
#else
        plasma_sgemm(
            transa, transb,
            m, n, k,
            alpha, A, lda,
                   B, ldb,
             beta, C, ldc);
#endif
    }

    plasma_time_t stop = omp_get_wtime();
    plasma_time_t time = stop-start;

    param[PARAM_TIME].d = time;
#ifdef COMPLEX
    param[PARAM_GFLOPS].d = flops_cgemm(m, n, k) / time / 1e9;
#else
    param[PARAM_GFLOPS].d = flops_sgemm(m, n, k) / time / 1e9;
#endif
    //================================================================
    // Test results by comparing to a reference implementation.
    //================================================================
    if (test) {

        float Anorm = LAPACKE_clange_work(
                           LAPACK_COL_MAJOR, 'F', Am, An, A,    lda, work);
        float Bnorm = LAPACKE_clange_work(
                           LAPACK_COL_MAJOR, 'F', Bm, Bn, B,    ldb, work);

        plasma_complex64_t *Ad =
            (plasma_complex64_t*)malloc((size_t)lda*An*sizeof(plasma_complex64_t));
        assert(Ad != NULL);
        plasma_clag2z(Am,An,A,   lda,Ad,lda);

        free(A);
        
        plasma_complex64_t *Bd =
            (plasma_complex64_t*)malloc((size_t)ldb*Bn*sizeof(plasma_complex64_t));
        assert(Bd != NULL);
        plasma_clag2z(Bm,Bn,B,   ldb,Bd,ldb);

        free(B);

        plasma_complex64_t *Cd =
            (plasma_complex64_t*)malloc((size_t)ldc*Cn*sizeof(plasma_complex64_t));
        assert(Cd != NULL);
        plasma_clag2z(Cm,Cn,Cref,ldc,Cd,ldc); // copy original matrix

        free(Cref);

        cblas_zgemm(
            CblasColMajor,
            (CBLAS_TRANSPOSE)transa, (CBLAS_TRANSPOSE)transb,
            m, n, k,
            CBLAS_SADDR(alpha), Ad, lda,
                                Bd, ldb,
            CBLAS_SADDR(beta),  Cd, ldc);

        free(Ad);
        free(Bd);

        plasma_complex64_t *Cout =
            (plasma_complex64_t*)malloc((size_t)ldc*Cn*sizeof(plasma_complex64_t));
        assert(Cout != NULL);

        plasma_clag2z(Cm,Cn,C,ldc,Cout,ldc);

        plasma_complex64_t zmone = -1.0;
        cblas_zaxpy((size_t)ldc*Cn, CBLAS_SADDR(zmone), Cd, 1, Cout, 1);

        double dwork[1];

        double error = LAPACKE_zlange_work(
                           LAPACK_COL_MAJOR, 'F', Cm, Cn, Cout, ldc, dwork);

        double normalize = ((double)Anorm) * ((double)Bnorm);

        if (normalize != 0)
            error /= normalize;

        param[PARAM_ERROR].d = error;
        param[PARAM_SUCCESS].i = error < 3*eps;

        free(Cd);
        free(Cout);

    }

    //================================================================
    // Free arrays.
    //================================================================

    free(C);
}

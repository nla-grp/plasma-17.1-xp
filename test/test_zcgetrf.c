/**
 *
 * @file
 *
 *  PLASMA is a software package provided by:
 *  University of Tennessee, US,
 *  University of Manchester, UK.
 *
 * @precisions mixed zc -> ds
 *
 **/
#include "test.h"
#include "flops.h"
#include "core_blas.h"
#include "core_lapack.h"
#include "plasma.h"

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <omp.h>

#define COMPLEX
#define A(i_, j_) A[(i_) + (size_t)lda*(j_)]

/***************************************************************************//**
 *
 * @brief Tests CPOTRF.
 *
 * @param[in]  param - array of parameters
 * @param[out] info  - string of column labels or column values; length InfoLen
 *
 * If param is NULL and info is NULL,     print usage and return.
 * If param is NULL and info is non-NULL, set info to column labels and return.
 * If param is non-NULL and info is non-NULL, set info to column values
 * and run test.
 ******************************************************************************/
void test_zcgetrf(param_value_t param[], char *info)
{
    //================================================================
    // Print usage info or return column labels or values.
    //================================================================
    if (param == NULL) {
        if (info == NULL) {
            // Print usage info.
            print_usage(PARAM_M);
            print_usage(PARAM_N);
            // print_usage(PARAM_PADA);
            print_usage(PARAM_NB);
            print_usage(PARAM_IB);
            print_usage(PARAM_NTPF);
            print_usage(PARAM_ZEROCOL);
            print_usage(PARAM_XP);
            print_usage(PARAM_COND);
            print_usage(PARAM_ERROR_FWD);
            print_usage(PARAM_ERROR_BWD);
        }
        else {
            // Return column labels.
            snprintf(info, InfoLen,
                     "%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s",
                     // "%*s %*s %*s %*s %*s %*s %*s",
                     InfoSpacing, "XP",
                     InfoSpacing, "COND",
                     InfoSpacing, "M",
                     InfoSpacing, "N",
                     // InfoSpacing, "PadA",
                     InfoSpacing, "NB",
                     InfoSpacing, "IB",
                     InfoSpacing, "NTPF",
                     InfoSpacing, "ZeroCol",
                     InfoSpacing, "ERRORFW",
                     InfoSpacing, "ERRORBW");
        }
        return;
    }
    // // Return column values.
    // snprintf(info, InfoLen,
    //          "%*c %*d %*d %*d %*d %*d %*d %f %f",
    //          InfoSpacing, param[PARAM_XP].c,
    //          InfoSpacing, param[PARAM_M].i,
    //          InfoSpacing, param[PARAM_N].i,
    //          // InfoSpacing, param[PARAM_PADA].i,
    //          InfoSpacing, param[PARAM_NB].i,
    //          InfoSpacing, param[PARAM_IB].i,
    //          InfoSpacing, param[PARAM_NTPF].i,
    //          InfoSpacing, param[PARAM_ZEROCOL].i,
    //          InfoSpacing, param[PARAM_ERROR_BWD].d,
    //          InfoSpacing, param[PARAM_ERROR_FWD].d);

    //================================================================
    // Set parameters.
    //================================================================
    int m = param[PARAM_M].i;
    int n = param[PARAM_N].i;

    int lda = imax(1, m+param[PARAM_PADA].i);

    int test = param[PARAM_TEST].c == 'y';
    float tol = param[PARAM_TOL].d * LAPACKE_slamch('E');
    double cond = param[PARAM_COND].d;

    //================================================================
    // Set tuning parameters.
    //================================================================
    plasma_set(PlasmaNb, param[PARAM_NB].i);
    plasma_set(PlasmaIb, param[PARAM_IB].i);
    plasma_set(PlasmaNumPanelThreads, param[PARAM_NTPF].i);

    //================================================================
    // Allocate and initialize arrays.
    //================================================================
    plasma_complex32_t *A =
        (plasma_complex32_t*)malloc((size_t)lda*n*sizeof(plasma_complex32_t));
    assert(A != NULL);

    int *ipiv = (int*)malloc((size_t)m*sizeof(int));
    assert(ipiv != NULL);

    int seed[] = {0, 0, 0, 1};
    lapack_int retval;


    if(cond>0.){

    // Allocate memory for the reference singular values
    float *Sref = (float *) malloc(imin(m,n)*sizeof(float));

    int mode = 4;
    float dmax = 1.0;
    // float cond = 1e4; // average-case condition number
    // float cond = 1e8; // worst-case condition number

    plasma_complex32_t *work =
      (plasma_complex32_t *)malloc(3*imax(m, n)* sizeof(plasma_complex32_t));

    // Initialize A with specific singular values
#ifdef COMPLEX
    LAPACKE_clatms_work(LAPACK_COL_MAJOR, m, n,
                        'U', seed, 'P', Sref, mode, cond,
                        dmax, m, n,'N', A, lda, work);
#else
    LAPACKE_slatms_work(LAPACK_COL_MAJOR, m, n,
                        'U', seed, 'P', Sref, mode, cond,
                        dmax, m, n,'N', A, lda, work);
#endif

    free(work);
    free(Sref);

    } else {

    retval = LAPACKE_clarnv(1, seed, (size_t)lda*n, A);
    assert(retval == 0);
    // for (int i = 0; i < n; i++) {
    //     A(i, i) = creal(A(i, i)) + n;
    //     for (int j = 0; j < i; j++) {
    //         A(j, i) = conjf(A(i, j));
    //     }
    // }

    }

    int zerocol = param[PARAM_ZEROCOL].i;
    if (zerocol >= 0 && zerocol < n)
        memset(&A[zerocol*lda], 0, m*sizeof(plasma_complex32_t));

    plasma_complex32_t *Aref = NULL;
    if (test) {
        Aref = (plasma_complex32_t*)malloc(
            (size_t)lda*n*sizeof(plasma_complex32_t));
        assert(Aref != NULL);

        memcpy(Aref, A, (size_t)lda*n*sizeof(plasma_complex32_t));
    }

    //================================================================
    // Run and time PLASMA.
    //================================================================
    plasma_time_t start = omp_get_wtime();

    int plainfo;
    
    if(param[PARAM_XP].c=='c')
    {
#ifdef COMPLEX
//     plainfo = plasma_cgetrf(m, n, A, lda, ipiv);
        plainfo = plasma_cgetrf_compensated(m, n, A, lda, ipiv);
#else
//     plainfo = plasma_sgetrf(m, n, A, lda, ipiv);
        plainfo = plasma_sgetrf_compensated(m, n, A, lda, ipiv);
#endif        
    }
    else if(param[PARAM_XP].c=='e')
    {
        plainfo = plasma_zcgetrf_extended(m, n, A, lda, ipiv);
    }
    else
    {
        plainfo = plasma_cgetrf(m, n, A, lda, ipiv);
    }

    


    plasma_time_t stop = omp_get_wtime();
    plasma_time_t time = stop-start;

    param[PARAM_TIME].d = time;
    param[PARAM_GFLOPS].d = flops_cgetrf(m, n) / time / 1e9;

    if(plainfo)
        printf("Problem in plasma facto\n");
    

    //================================================================
    // Test results by comparing to a reference implementation.
    // This will give spurious failures if LAPACK picks different pivots
    // than PLASMA. Should test solve or ||PA - LU||.
    //================================================================
    if (test) {

        int lapinfo;

        // Store result in double precision
        plasma_complex64_t *Ad =
            (plasma_complex64_t*)malloc((size_t)lda*n*sizeof(plasma_complex64_t));
        assert(Ad != NULL);
        plasma_clag2z(m,n,A,lda,Ad,lda);

        free(A);

        // Solve linear system in double precision
        int nrhs = 1;

        // ... in order to compute fowrward error we need
        //     to sample x and compute b as Arefd * x

        // ... sample xex in single precision and store in fp64 array
        plasma_complex32_t *xexf =
            (plasma_complex32_t*)malloc((size_t)nrhs*n*sizeof(plasma_complex32_t));

        // LAPACKE_clarnv(1, seed, (size_t)nrhs*n, xexf);
        for (int i = 0; i < nrhs*n; ++i)
        {
            xexf[i] = 1.;
        }
        plasma_complex64_t *xex =
            (plasma_complex64_t*)malloc((size_t)nrhs*n*sizeof(plasma_complex64_t));
        plasma_clag2z(n,nrhs,xexf,lda,xex,lda);
        
        free(xexf);

        double work[1];
        double xexnorm = LAPACKE_zlange_work(
            LAPACK_COL_MAJOR, 'F', n, nrhs, xex, lda, work);
        // printf("xexnorm=%e\n", xexnorm);

        // Convert Aref to double precision
        plasma_complex64_t *Aref_d =
            (plasma_complex64_t*)malloc((size_t)lda*n*sizeof(plasma_complex64_t));
        assert(Aref_d != NULL);
        plasma_clag2z(m,n,Aref,lda,Aref_d,lda);

        // free(Aref);

        // Norm of initial A
        double Anorm = LAPACKE_zlange_work(
            LAPACK_COL_MAJOR, 'F', m, n, Aref_d, lda, work);

        // Compute  b = Ax
        plasma_complex64_t *b =
            (plasma_complex64_t*)malloc((size_t)nrhs*n*sizeof(plasma_complex64_t));
        
        plasma_zgemm(
            PlasmaNoTrans, PlasmaNoTrans,
            n, nrhs, n,
            1.0, Aref_d, lda,
                  xex, lda,
            0.0, b, lda);

        // Compute norm of b 
        double bnorm = LAPACKE_zlange_work(
            LAPACK_COL_MAJOR, 'F', n, nrhs, b, lda, work);
        // printf("bnorm=%e\n", bnorm);

        // Find solution of Ad x = b 
        plasma_complex64_t *x =
            (plasma_complex64_t*)malloc((size_t)nrhs*n*sizeof(plasma_complex64_t));
        memcpy(b, x, (size_t)nrhs*n*sizeof(plasma_complex64_t));

        plasma_zgetrs(n, nrhs, Ad, lda, ipiv, x, lda);

        if(retval)
            printf("Something went wrong in the solve...\n");


        // plasma_zlaswp(PlasmaRowwise, n, nrhs, x, lda, ipiv, 1);

        // Compute norm of x in double precision
        double xnorm = LAPACKE_zlange_work(
            LAPACK_COL_MAJOR, 'F', n, nrhs, x, lda, work);
        // printf("xnorm=%e\n", xnorm);

        // printf("x=[");
        // for (int i = 0; i < n; ++i)
        // {
        //     printf("%e ", x[i]);
        // }
        // printf("]\n");


        // Compute backward error b = b - Ax
        plasma_zgemm(
            PlasmaNoTrans, PlasmaNoTrans,
            n, nrhs, n,
            -1.0, Aref_d, lda,
                  x, lda,
            +1.0, b, lda);

        double bwderr = LAPACKE_zlange_work(
            LAPACK_COL_MAJOR, 'F', n, nrhs, b, lda, work);

        // Compute forward error x = xex - x
        plasma_complex64_t zmone = -1.0;
        cblas_zaxpy((size_t)n*nrhs, CBLAS_SADDR(zmone), xex, 1, x, 1);

        double fwderr = LAPACKE_zlange_work(
            LAPACK_COL_MAJOR, 'F', n, nrhs, x, lda, work);
        // printf("fwderr=%e\n", fwderr);

        // Normalize errors
        // bwderr /= (Anorm*xnorm+bnorm);
        fwderr /= (xnorm);

        // // Double prec LU
        // lapinfo = LAPACKE_zgetrf(
        //     LAPACK_COL_MAJOR,
        //     m, n,
        //     Aref_d, lda, ipiv);

        free(Aref_d);

        // Compute error: pA - LU
        plasma_complex64_t *L =
            (plasma_complex64_t*)malloc((size_t)n*n*sizeof(plasma_complex64_t));
        plasma_complex64_t *U =
            (plasma_complex64_t*)malloc((size_t)n*n*sizeof(plasma_complex64_t));
        
        // plasma_zlaset(PlasmaGeneral,n,n,0.0,1.0,L, lda);
        plasma_zlaset(PlasmaGeneral,n,n,0.0,0.0,L, lda);
        plasma_zlaset(PlasmaGeneral,n,n,0.0,0.0,U, lda);
    
        // Assemble L and U, 
        plasma_zlacpy(PlasmaLower,n,n,Ad, lda, L, lda);
        plasma_zlacpy(PlasmaUpper,n,n,Ad, lda, U, lda);
        for (int j=0; j < n; j++)
            L[j+j*lda] = 1.0;

        // printf("L=[");
        // for (int i = 0; i < n; ++i) {
        //     for (int j = 0; j < n; ++j)
        //     {
        //         printf("%e ", L[i+j*lda]);
        //     }
        //     printf("\n");
        // }
        // printf("]\n");

        // printf("U=[");
        // for (int i = 0; i < n; ++i) {
        //     for (int j = 0; j < n; ++j)
        //     {
        //         printf("%e ", U[i+j*lda]);
        //     }
        //     printf("\n");
        // }
        // printf("]\n");

        plasma_clag2z(m,n,Aref,lda,Ad,lda);

        plasma_zlaswp(PlasmaRowwise, n, n, Ad, lda, ipiv, 1);

        // printf("Ad=[");
        // for (int i = 0; i < n; ++i) {
        //     for (int j = 0; j < n; ++j)
        //     {
        //         printf("%e ", Ad[i+j*lda]);
        //     }
        //     printf("\n");
        // }
        // printf("]\n");

        double Lnorm = LAPACKE_zlange_work(
            LAPACK_COL_MAJOR, 'F', m, n, L, lda, work);
        double Unorm = LAPACKE_zlange_work(
            LAPACK_COL_MAJOR, 'F', m, n, U, lda, work);


        bwderr /= (Lnorm*Unorm*xnorm);


        // Compute L times U 
        plasma_zgemm(
            PlasmaNoTrans, PlasmaNoTrans,
            n, n, n,
            -1.0, L, lda,
                  U, lda,
            1.0, Ad, lda);

        free(L);
        free(U);

        double error = LAPACKE_zlange_work(
            LAPACK_COL_MAJOR, 'F', m, n, Ad, lda, work);

        free(Ad);
    
        // error /= Anorm + Lnorm*Unorm;
        error /= Lnorm*Unorm;

        // double error = 0.0;
        
        param[PARAM_ERROR_FWD].d = fwderr;
        param[PARAM_ERROR_BWD].d = bwderr;
        param[PARAM_ERROR].d = error;
        param[PARAM_SUCCESS].i = error < tol;

    }

    // Return column values.
    snprintf(info, InfoLen,
             "%*c %*e %*d %*d %*d %*d %*d %*d %*e %*e",
             InfoSpacing, param[PARAM_XP].c,
             InfoSpacing, param[PARAM_COND].d,
             InfoSpacing, param[PARAM_M].i,
             InfoSpacing, param[PARAM_N].i,
             // InfoSpacing, param[PARAM_PADA].i,
             InfoSpacing, param[PARAM_NB].i,
             InfoSpacing, param[PARAM_IB].i,
             InfoSpacing, param[PARAM_NTPF].i,
             InfoSpacing, param[PARAM_ZEROCOL].i,
             InfoSpacing, param[PARAM_ERROR_FWD].d,
             InfoSpacing, param[PARAM_ERROR_BWD].d);

    //================================================================
    // Free arrays.
    //================================================================
    // free(A);
    free(ipiv);
    if (test)
        free(Aref);
}

/**
 *
 * @file
 *
 *  PLASMA is a software package provided by:
 *  University of Tennessee, US,
 *  University of Manchester, UK.
 *
 * @precisions mixed zc -> ds
 *
 **/
#ifndef ICL_CORE_BLAS_ZC_H
#define ICL_CORE_BLAS_ZC_H

#include "plasma_async.h"
#include "plasma_types.h"
#include "plasma_workspace.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************/
void core_zlag2c(int m, int n,
                 plasma_complex64_t *A,  int lda,
                 plasma_complex32_t *As, int ldas);

void core_clag2z(int m, int n,
                 plasma_complex32_t *As, int ldas,
                 plasma_complex64_t *A,  int lda);

/******************************************************************************/
void core_omp_zlag2c(int m, int n,
                     plasma_complex64_t *A,  int lda,
                     plasma_complex32_t *As, int ldas,
                     plasma_sequence_t *sequence, plasma_request_t *request);

void core_omp_clag2z(int m, int n,
                     plasma_complex32_t *As, int ldas,
                     plasma_complex64_t *A,  int lda,
                     plasma_sequence_t *sequence, plasma_request_t *request);

void core_omp_zcgemm_extended(
    plasma_enum_t transa, plasma_enum_t transb,
    int m, int n, int k,
    plasma_complex32_t alpha, const plasma_complex32_t *A, int lda,
                              const plasma_complex32_t *B, int ldb,
    plasma_complex64_t beta,        plasma_complex64_t *C, int ldc,
    plasma_complex32_t *C32, int ldc32,
    // plasma_complex64_t *C64, int ldc64,
    plasma_sequence_t *sequence, plasma_request_t *request);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif // ICL_CORE_BLAS_ZC_H

/**
 *
 * @file
 *
 *  PLASMA is a software package provided by:
 *  University of Tennessee, US,
 *  University of Manchester, UK.
 *
 * @precisions mixed zc -> ds
 *
 **/
#ifndef ICL_PLASMA_INTERNAL_ZC_H
#define ICL_PLASMA_INTERNAL_ZC_H

#include "plasma_async.h"
#include "plasma_descriptor.h"
#include "plasma_types.h"
#include "plasma_workspace.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************/
void plasma_pzlag2c(plasma_desc_t A, plasma_desc_t As,
                    plasma_sequence_t *sequence, plasma_request_t *request);

void plasma_pclag2z(plasma_desc_t As, plasma_desc_t A,
                    plasma_sequence_t *sequence, plasma_request_t *request);


void plasma_pzcgemm_extended(plasma_enum_t transa, plasma_enum_t transb,
                   plasma_complex32_t alpha, plasma_desc_t A,
                                             plasma_desc_t B,
                   plasma_complex64_t beta,  plasma_desc_t C, plasma_desc_t D, int freq,
                   plasma_sequence_t *sequence, plasma_request_t *request);

void plasma_pzcgetrf_extended(plasma_desc_t A, int *ipiv, plasma_desc_t Z,
                     plasma_sequence_t *sequence, plasma_request_t *request);


#ifdef __cplusplus
}  // extern "C"
#endif

#endif // ICL_PLASMA_INTERNAL_ZC_H
